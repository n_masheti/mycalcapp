package com.nevillemasheti.mycalcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button btnAdd, btnMultiply, btnSubtract, btnDivide;
    EditText etFirst, etSecond, etResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSubtract = (Button) findViewById(R.id.btnSubtract);
        btnDivide = (Button) findViewById(R.id.btnDivide);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);

        etFirst = (EditText) findViewById(R.id.et_first);
        etResult = (EditText) findViewById(R.id.et_result);
        etSecond = (EditText) findViewById(R.id.et_second);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(Integer.parseInt(etFirst.getText().toString()) , Integer.parseInt(etSecond.getText().toString()) );

            }
        });

        btnSubtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                substract(Integer.parseInt(etFirst.getText().toString()) , Integer.parseInt(etSecond.getText().toString()) );

            }
        });

        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiply(Integer.parseInt(etFirst.getText().toString()) , Integer.parseInt(etSecond.getText().toString()) );

            }
        });

        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                divide(Integer.parseInt(etFirst.getText().toString()) , Integer.parseInt(etSecond.getText().toString()) );

            }
        });

    }

    private void add(int f, int s)
    {
        int result = f + s;
        etResult.setText(String.valueOf(result));
    }

    private void substract(int f, int s)
    {
        int result = f - s;
        etResult.setText(String.valueOf(result));
    }

    private void multiply(int f, int s)
    {
        int result = f * s;
        etResult.setText(String.valueOf(result));
    }

    private void divide(int f, int s)
    {
        int result = f / s;
        etResult.setText(String.valueOf(result));
    }
}
